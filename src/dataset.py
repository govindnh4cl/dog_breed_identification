import numpy as np
import pandas as pd
import seaborn
import matplotlib.pyplot as plt

# User defined imports
from config import config

def plot_breed_distribution():
    df = pd.read_csv(config['label_file_path'])
    freq = df['breed'].value_counts()
    plt.figure()
    seaborn.barplot(x=freq.index, y=freq)
    plt.xticks(rotation=90)
    plt.show(block=False)
    plt.figure()
    freq_sorted = freq.sort_index()
    seaborn.barplot(x=freq_sorted.index, y=freq_sorted)
    plt.xticks(rotation=90)
    plt.show(block=False)


if __name__ == '__main__':
    plot_breed_distribution()

    plt.show(block=True) # Block the execution of programe